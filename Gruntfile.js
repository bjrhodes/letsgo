/**
 * AIM: we want 3 different configs here.
 *  One for developing things using watch which should produce unminified, concated output
 *  One to build for development which produces minified concat output
 *  One to build for production which produces versioned, minified concat, gzipped output.
 *
 *  Should, in all three, build and (if appropriate) version scss too.
 */

/*global module:false*/
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt, {pattern: ['grunt-*', '!*istanbul'], scope: 'devDependencies'});

  function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*.js', {cwd: path}).forEach(function(option) {
      key = option.replace(/\.js$/,'');
      object[key] = require(path + option);
    });

    return object;
  }

  var pkg = grunt.file.readJSON('package.json');
  pkg.version = grunt.option('packageversion') || pkg.version;

  grunt.initConfig({pkg: pkg});

  grunt.extendConfig(loadConfig('./build/grunt-config/common/'));
  grunt.extendConfig(loadConfig('./build/grunt-config/php/'));

  grunt.registerTask('init', ['clean:coverage', 'shell:composer', 'default']);

  grunt.registerTask('testphp', ['phplint', 'phpunit', 'phpcs'/*, 'phpmd' PHPMD is currently suffering a dpendency issue on composer. Can't be bothered to fix yet. 08/06/14 */]);

  grunt.registerTask('default', ['testphp', 'todo'])
};
