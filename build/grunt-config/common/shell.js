module.exports = {
  composer: {
    options: {
      stdout: true,
      stderr: true,
      failOnError: true
    },
    command: 'composer install'
  }
}
