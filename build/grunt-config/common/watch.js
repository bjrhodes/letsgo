module.exports = {
  php: {
    files: ['src/**/*.php', '!**/vendor/**'],
    tasks: ['testphp']
  },
  phpunit: {
    files: ['tests/**/*.php'],
    tasks: ['testphp']
  }
}
