module.exports = {
  options: {
    marks: [
      {
        pattern: "todo",
        color: "yellow"
      }
    ]
  },
  src: ['src/**/*', '!**/vendor/**/*']
}
