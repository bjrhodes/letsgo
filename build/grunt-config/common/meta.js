module.exports = {
  banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
          '* http://insidefoundry.co.uk/\n' +
          '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
          'insidefoundry.co.uk */\n'
}
