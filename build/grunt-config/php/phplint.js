module.exports = {
  tests: ['tests/**/*.php'],
  app: ['src/app/**/*.php'],
  all: ['src/app/**/*.php', 'tests/**/*.php']
}
