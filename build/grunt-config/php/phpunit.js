module.exports = {
  classes: {
    dir: 'tests'
  },
  options: {
    configuration: 'tests/phpunit-no-coverage.xml',
    colors: true,
    stopOnFailure: true,
    followOutput: true,
    bin: 'src/vendor/bin/phpunit'
  }
}
