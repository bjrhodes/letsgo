module.exports = {
  application: {dir: ['src/app']},
  options: {
    standard: 'build/PHPCS/ruleset.xml',
    ignore: 'vendor',
    bin: 'src/vendor/bin/phpcs'
  }
}
