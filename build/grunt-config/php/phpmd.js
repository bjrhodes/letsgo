module.exports = {
  application: {dir: 'src'},
  options: {
    reportFormat: 'text',
    exclude: ['vendor'],
    rulesets: 'build/phpmd.xml',
    bin: 'src/vendor/bin/phpmd'
  }
}
