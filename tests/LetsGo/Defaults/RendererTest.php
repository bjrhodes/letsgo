<?php
namespace MML\LetsGo\Defaults;


class RendererTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Renderer
     */
    protected $Object;

    protected $Factory;
    protected $Config;
    protected $Request;

    protected $Response;
    protected $View;
    protected $User;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->Factory = $this->getMock('\\MML\\LetsGo\\Interfaces\\Factory');

        $this->Request = $this->getMockBuilder('\\MML\\LetsGo\\Interfaces\\Request')
                ->disableOriginalConstructor()
                ->getMock();
        $this->Response = $this->getMockBuilder('\\MML\\LetsGo\\Interfaces\\Response')
                ->disableOriginalConstructor()
                ->getMock();
        $this->View = $this->getMockBuilder('\\MML\\LetsGo\\Views\\Html')
                ->disableOriginalConstructor()
                ->getMock();
        $this->Config = $this->getMockBuilder('\\MML\\LetsGo\\Interfaces\\Config')
                ->disableOriginalConstructor()
                ->getMock();
        $this->User = $this->getMockBuilder('\\MML\\LetsGo\\Interfaces\\User')
                ->disableOriginalConstructor()
                ->getMock();

        $this->Factory->expects($this->any())->method('getConfig')->will($this->returnValue($this->Config));

        $this->Object = new Renderer($this->Factory);
    }

    public function minPass()
    {
        $this->Config->expects($this->any())
                ->method('has')
                ->will($this->returnValue(false));

        $this->Config->expects($this->any())
                ->method('get')
                ->with('defaultLayout')
                ->will($this->returnValue('DEFAULT THIS BABY'));

        $this->Request->expects($this->any())
                ->method('has')
                ->will($this->returnValue(false));

        $this->Response->expects($this->any())
                ->method('output')
                ->will($this->returnValue(array(
                    array('requestId' => 'mockRequest', 'data' => array('some response'))
                )));

        $this->Factory->expects($this->once())->method('getView')->with('Html', $this->Request)->will($this->returnValue($this->View));
    }

    /**
     */
    public function testRenderReturnsView()
    {
        $this->minPass();

        $this->assertEquals($this->View, $this->Object->render($this->Response, $this->Request, $this->User));
    }

    /**
     */
    public function testRenderPopulatesView()
    {
        $this->minPass();

        $this->View->expects($this->any())
                ->method('populate');

        $this->Object->render($this->Response, $this->Request, $this->User);
    }

    /**
     */
    public function testViewMapIsCheckedByRender()
    {
        $map = array(
            'workflowName' => array(
                'view' => 'Html',
                'template' => 'someUniqueTemplate'
            )
        );

        $this->minPass();

        $this->View->expects($this->once())
                ->method('template')
                ->with('someUniqueTemplate.php');
        $this->View->expects($this->once())
                ->method('populate');
        $this->Response->expects($this->once())
                ->method('workflow')
                ->will($this->returnValue('workflowName'));

        $this->Object->setMapping($map);

        $this->Object->render($this->Response, $this->Request, $this->User);
    }

    public function testMultiRequestUsesJsonView()
    {
        $this->Factory->expects($this->once())->method('getView')->with('Json', $this->Request)->will($this->returnValue($this->View));
        $this->Response->expects($this->once())
                ->method('count')
                ->will($this->returnValue(2));

        $this->Object->render($this->Response, $this->Request, $this->User);
    }

}
