# Barebones

A quick-start skeleton for multiple project types.

## Getting Started

### Dependencies

You should have installed:

 *  nodejs [http://nodejs.org]
 *  gruntjs [http://gruntjs.com] `npm install -g grunt-cli`
 *  bower [http://bower.io] `npm install -g bower`

For PHP:

 *  composer [http://getcomposer.org]

For SASS:

 *  ruby [https://www.ruby-lang.org/]
 *  compass [http://compass-style.org/] `gem install compass`

Optionally:

 *  susy [http://susy.oddbird.net] `gem install susy`

### Usage

Clone the repository

`npm install`

`grunt init`

Then put your code in `src/` in the relevant directory.

To run your app locally make `server/public_html` your docroot.

For rapid development use `grunt watch`

You will need to replace the \Acme namespace with that of your application for autoloading.