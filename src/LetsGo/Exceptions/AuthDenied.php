<?php
namespace MML\LetsGo\Exceptions;

/**
 * Description of AuthDenied
 *
 * @author barry
 */
class AuthDenied extends \MML\LetsGo\Exceptions\Base
{
    public function httpStatus()
    {
        return '401';
    }
    public function httpMessage()
    {
        return 'Permission denied';
    }
}
