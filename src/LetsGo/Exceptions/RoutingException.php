<?php
namespace MML\LetsGo\Exceptions;

/**
 * Description of RoutingException
 *
 * @author barry
 */
class RoutingException extends Base
{
    public function httpStatus()
    {
        return '404';
    }
    public function httpMessage()
    {
        return 'Not Found';
    }
}
