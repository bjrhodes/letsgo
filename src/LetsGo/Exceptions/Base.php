<?php

namespace MML\LetsGo\Exceptions;

/**
 * Description of BAse
 *
 * @author barry
 */
class Base extends \Exception
{
    public function httpStatus()
    {
        return '500';
    }
    public function httpMessage()
    {
        return 'Application error.';
    }

    public function getLogMessage()
    {
        return 'Exception in ' . $this->getFile() . ' [' . $this->getLine() . '] : ' . $this->getMessage();
    }

    public function log()
    {
        error_log($this->getLogMessage());
    }
}
