<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of TemplateRenderer
 *
 * @author barry
 */
interface TemplateRenderer
{
    public function setTemplate($template);
    public function render(array $vars);
}
