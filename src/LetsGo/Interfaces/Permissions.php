<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of Permissions
 *
 * @author barry
 */
interface Permissions
{
    /**
     * @param string $token
     *
     * @return bool true if user has permission, false otherwise
     */
    public function has($token);
}
