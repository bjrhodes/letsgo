<?php
namespace MML\LetsGo\Interfaces;

/**
 * This is the main factory interface.
 *
 * To start customising functionality, create a new factory meeting this interface.
 * If you only want to customise a little, we recommend extending the abstract class
 * MML\LetsGo\Defaults\AbstractFactory
 *
 * @author barry
 */
interface Factory
{
    public function get($class, array $params = null);
    public function getConfig();
    public function getRequest($url = null, array $post = null, array $get = null, array $server = null);
    public function getRouter($type = null);
    public function getRenderer();
    public function getResponse();
    public function getAuthentication();
    public function getFrontController();
    public function getWorkflow($workflowName);
    public function getView($viewName, $Request);

    /**
     * Used to set the parent, so the users factory can create instances of other objects with the Factory at the head
     * of the tree, with full knowledge of all namespaces.
     */
    //public function parentFactory(Factory $Factory);
}
