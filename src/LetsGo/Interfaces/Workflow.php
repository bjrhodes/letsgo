<?php

namespace MML\LetsGo\Interfaces;

/**
 *
 * @author barry
 */
interface Workflow
{
    public function __construct(Factory $Factory);
    public function run(Request $Request, User $User);

    /**
     * OPTIONAL METHOD
     *
     * If your workflow specifically requires a viewtype and won't work without it, let us know in this method
     *
     * @return string type of workflow
     */
    //public function viewtype();

    /**
     * OPTIONAL METHOD
     *
     * To override the default application layout (set in Config defaults,) define this function in your workflow.
     *
     * @return string layout template.
     */
    //public function layout();
}
