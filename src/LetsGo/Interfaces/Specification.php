<?php
namespace MML\LetsGo\Interfaces;

/**
 * Authorisation specification object.
 *
 * @author barry
 */
interface Specification
{
    public function isSatisfiedBy(User $User);
}
