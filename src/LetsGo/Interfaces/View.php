<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of View
 *
 * @author barry
 */
interface View
{
    /**
     * Populates the View with any parameters it will need.
     *
     * @param array $contents
     */
    public function populate(array $contents);

    /**
     * Triggers the views render function
     */
    public function render();
}
