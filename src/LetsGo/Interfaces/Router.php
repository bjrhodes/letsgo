<?php
namespace MML\LetsGo\Interfaces;


/**
 * @author barry
 */
interface Router
{
    /**
     *
     * @param \MML\LetsGo\Interfaces\Request $Request
     * @param \MML\LetsGo\Interfaces\User $User
     *
     * @return array of \MML\LetsGo\Interfaces\Workflows
     */
    public function route(Request $Request, User $User);
}
