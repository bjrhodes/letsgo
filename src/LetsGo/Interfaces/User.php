<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of User
 *
 * @author barry
 */
interface User
{
    public function username();

    /**
     * Returns a Permissions object, optionally specify a realm depending on your implementation
     *
     * @param mixed $realm
     *
     * @return Permissions
     */
    public function permissions($realm = null);

    /**
     * getter / setter for an authtoken
     *
     * @param type $newToken
     */
    public function authToken($newToken = null);
}
