<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of Config
 *
 * @author barry
 */
interface Config
{
    public function get($key);
    public function has($key);
}
