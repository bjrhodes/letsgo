<?php
namespace MML\LetsGo\Interfaces;

/**
 * An authentication object should return a user based on a request object. Either from an
 * existing session or from submitted login details. This object should not handle authorisation.
 * This is seperatly managed by Sepificiations and Permissions.
 *
 * @author barry
 */
interface Authentication
{
    public function getUser(Request $Request);
}
