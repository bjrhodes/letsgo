<?php
namespace MML\LetsGo\Interfaces;

/**
 * Description of Response
 *
 * @author barry
 */
interface Response extends \Iterator
{
    /**
     *
     * @param array $out The output from the request
     * @param string $workflowName The workflow that generated the output
     * @param string $requestIdentifier request identifier
     */
    public function addOutput(array $out, $workflowName, $requestIdentifier);
    public function getById($id);

    public function output();
    public function workflow();
    public function id();
    public function count();
}
