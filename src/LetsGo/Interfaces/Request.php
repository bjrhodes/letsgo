<?php

namespace MML\LetsGo\Interfaces;

/**
 * @author barry
 */
interface Request
{
    public function __construct(array $post, array $get, array $server, $url);
    public function has($key);
    public function get($key);
    public function hasPost($key);
    public function getPost($key);
    public function hasOption($key);
    public function getOption($key);
    public function hasServer($key);
    public function getServer($key);
    public function url();
    public function barePath();
}
