<?php
namespace MML\LetsGo\Interfaces;

/**
 * I think of this as a Router at the other end of the app. Router routes in, Renderer routes out. Links up workflows
 * and requests with views.
 *
 * @author barry
 */
interface Renderer
{
    public function __construct(Factory $Factory);
    public function render(Response $Response, Request $Request, User $User);
    public function setMapping(array $map);
}
