<?php
namespace MML\LetsGo\Workflows;

use \MML\LetsGo\Interfaces;
use \MML\LetsGo\Exceptions;

/**
 * Description of NotFoundWorkflow
 *
 * @author barry
 */
class NotFound implements Interfaces\Workflow
{
    protected $Factory;

    public function __construct(Interfaces\Factory $Factory)
    {
        $this->Factory = $Factory;
    }

    public function run(Interfaces\Request $Request, Interfaces\User $User)
    {
        throw new Exceptions\RoutingException("Resource Not Found", 404);
    }
}
