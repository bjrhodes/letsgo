<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;

/**
 * Description of DefaultRenderer
 *
 * @author barry
 */
class Renderer implements Interfaces\Renderer
{
    protected $Factory;
    protected $Config;

    protected $viewMap = array();

    public function __construct(Interfaces\Factory $Factory)
    {
        $this->Factory = $Factory;
        $this->Config = $this->Factory->getConfig();
    }

    /**
     * Probes Request , Config and Workflow for the type of view that should be used.
     *
     * @param \MML\LetsGo\Interfaces\Workflow $Workflow
     * @param \MML\LetsGo\Interfaces\Request $Request
     * @return View
     */
    public function render(Interfaces\Response $Response, Interfaces\Request $Request, Interfaces\User $User)
    {
        if ($Response->count() > 1) {

            $View = $this->Factory->getView('Json', $Request);

            $output = $this->collateOutput($Response, $User);

        } else {
            // Workflow is in the form returned by get_class for the object
            $workflow = $Response->workflow();

            if (array_key_exists($workflow, $this->viewMap) && is_array($this->viewMap[$workflow])) {
                $map = $this->viewMap[$workflow];
            } else {
                $map = array();
            }

            $View = $this->getView($map, $Request);

            if (is_callable(array($View, 'template'))) {
                $View->template($this->getTemplate($map, $workflow));
            }
            if (is_callable(array($View, 'layout'))) {
                $View->layout($this->getLayout($map));
            }

            // @todo. Next "Standard Web" app, modify this so that it will setup Cookies here, OR merge into output.
            $output = array_merge(array('authToken' => $User->authToken()), $Response->output());
        }

        $View->populate($output);

        return $View;
    }

    /**
     *
     * @param array $map should be of the form:
     * [
     *  {
     *      workflowName : {
     *          'view'      : viewtype,
     *          'template'  : templateName,
     *          'layout'    : layoutName,
     *      }
     *  }
     * ]
     * where each of view,layout and template are optional
     */
    public function setMapping(array $map)
    {
        $this->viewMap = $map;
    }

    protected function collateOutput(Interfaces\Response $Response, Interfaces\User $User)
    {
        $out = array(
            'success' => true,
            'authToken' => $User->authToken()
        );

        foreach ($Response as $Partial) {
            $out[$Partial->id()] = $Partial->output();
        }

        return $out;
    }

    protected function getView(array $map, Interfaces\Request $Request)
    {
        // default to html
        $viewtype = 'Html';

        // see if the config knows better
        if ($this->Config->has('defaultView')) {
            $viewtype = $this->Config->get('defaultView');
        }

        // see if the request knows better
        if ($Request->has('viewtype') && is_string($Request->get('viewtype'))) {
            $viewtype = $Request->get('viewtype');
        }

        // see if the response knows better
        if (array_key_exists('view', $map)) {
            $viewtype = $map['view'];
        }

        return $this->Factory->getView($viewtype, $Request);
    }

    protected function getLayout(array $map)
    {
        $layout = $this->Config->get('defaultLayout');

        // Override with the viewmap if set
        if (array_key_exists('layout', $map)) {
            $layout = $map['layout'];
        }

        return $layout . '.php';
    }

    protected function getTemplate(array $map, $workflowName)
    {
        if (strstr($workflowName, '\\')) {
            $template = lcfirst(substr($workflowName, strrpos($workflowName, '\\') + 1));
        } else {
            $template = lcfirst($workflowName);
        }

        // Override with the viewmap if set
        if (array_key_exists('template', $map)) {
            $template = $map['template'];
        }

        return $template . '.php';
    }
}
