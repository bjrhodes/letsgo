<?php

namespace MML\LetsGo\Defaults;

/**
 * This is the default config, if you have different needs, keys passed in will override the defaults.
 *
 * Preset defaults are:
 *
 * moduleRoot (Probably best to leave this on be!)
 * siteTitle
 * cdn
 * defaultView
 * defaultTemplateRoot
 * templatesDir
 * cssFiles
 * jsFiels
 *
 *
 * @author barry
 */
class Config implements \MML\LetsGo\Interfaces\Config
{
    // these should probably be over-ridden by you, Mr End User
    protected $moduleRoot;
    protected $siteTitle = 'Default';
    protected $cdn = '/';
    protected $defaultView = 'Html';
    protected $defaultLayout = 'application';
    protected $templatesDir;
    protected $layoutsDir;
    protected $cssFiles = array();
    protected $jsFiles = array();

    /**
     * @param type $json
     */
    public function __construct(array $vars, array $searchReplaceVars = array())
    {
        $this->moduleRoot = dirname(__DIR__) . DIRECTORY_SEPARATOR;
        $this->defaultTemplatesRoot = $this->moduleRoot . 'templates' . DIRECTORY_SEPARATOR;
        $this->defaultLayoutsRoot = $this->moduleRoot . 'layouts' . DIRECTORY_SEPARATOR;
        $this->templatesDir = $this->defaultTemplatesRoot;
        $this->layoutsDir = $this->defaultLayoutsRoot;

        foreach ($vars as $k => $v) {
            foreach ($searchReplaceVars as $pattern => $replace) {
                $v = str_replace($pattern, $replace, $v);
            }

            $this->$k = $v;
        }
    }

    public function get($key)
    {
        return $this->$key;
    }

    public function has($key)
    {
        return property_exists($this, $key);
    }
}
