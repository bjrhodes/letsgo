<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;

/**
 * Description of Request
 *
 * @author barry
 */
class Request implements Interfaces\Request
{
    protected $post;
    protected $options;
    protected $server;
    protected $url;

    public function __construct(array $post, array $get, array $server, $url)
    {
        $this->post     = $post;
        $this->options  = $get;
        $this->server   = $server;
        $this->url      = $url;

        $this->parseUrl();
    }

    /**
     * Checks Post, GET and SERVER for the requested key. Prefers keys found in that order
     *
     * @param string $key
     * @return mixed
     *
     * @throws Exceptions\RequestException if no key is found
     */
    public function get($key)
    {
        if ($this->hasPost($key)) {
            $val = $this->getPost($key);
        } elseif ($this->hasOption($key)) {
            $val = $this->getOption($key);
        } elseif ($this->hasServer($key)) {
            $val = $this->getServer($key);
        } else {
            throw new Exceptions\RequestException('Missing requested key');
        }

        return $val;
    }

    /**
     * Checks Post, GET and SERVER for the requested key.
     *
     * @param type $key
     * @return type
     */
    public function has($key)
    {
        return ($this->hasPost($key) || $this->hasOption($key) || $this->hasServer($key));
    }

    /**
     * returns the url as submitted to the class
     *
     * @return type
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * Returns the url stripped of all options / get data and decoded
     *
     * @return string
     */
    public function barePath()
    {
        $path = $this->stripGet();

        $matches = array();
        $pattern = '#(/[^/:]*:[^/]*)#i';

        preg_match_all($pattern, $path, $matches);

        if (is_array($matches[1])) {
            foreach ($matches[1] as $match) {
                $path = str_replace($match, '', $path);
            }
        }

        return rtrim(rawurldecode($path), '/');
    }

    /**
     * Checks for $key inside $_POST submitted
     *
     * @param string $key
     * @return bool
     */
    public function hasPost($key)
    {
        return array_key_exists($key, $this->post);
    }

    /**
     * Returns $key from $_POST submitted
     *
     * @param string $key
     * @return mixed
     */
    public function getPost($key)
    {
        return $this->post[$key];
    }

    /**
     * Checks for $key inside $_GET submitted and any options passed on the url
     *
     * @param string $key
     * @return bool
     */
    public function hasOption($key)
    {
        return array_key_exists($key, $this->options);
    }

    /**
     * Returns $key from $_GET submitted or any url options found
     *
     * @param string $key
     * @return mixed
     */
    public function getOption($key)
    {
        return $this->options[$key];
    }

    /**
     * Checks for $key inside $_SERVER as submitted
     *
     * @param string $key
     * @return bool
     */
    public function hasServer($key)
    {
        return array_key_exists($key, $this->server);
    }

    /**
     * Returns $key from $_SERVER submitted
     *
     * @param string $key
     * @return mixed
     */
    public function getServer($key)
    {
        return $this->server[$key];
    }

    protected function parseUrl()
    {
        $path = $this->stripGet();
        $matches = array();
        $options = array();

        $pattern = '#(?:/([^/:]*):([^/]*))#i';

        preg_match_all($pattern, $path, $matches);

        if (is_array($matches[1])) {
            foreach ($matches[1] as $index => $key) {
                $options[strtolower($key)] = $matches[2][$index];
            }
        }

        $this->options = array_merge($options, $this->options);
    }

    protected function stripGet()
    {
        if (strstr($this->url, '?')) {
            $path = substr($this->url, 0, strrpos($this->url, '?'));
        } else {
            $path = $this->url;
        }

        return $path;
    }
}
