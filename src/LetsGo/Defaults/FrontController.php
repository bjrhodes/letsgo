<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;

/**
 * Description of FrontController
 *
 * @author barry
 */
class FrontController
{
    protected $Factory;

    public function __construct(Interfaces\Factory $Factory)
    {
        $this->Factory = $Factory;
    }

    /**
     *
     * @param array $publicRoutes Routes which are always allowed.
     * @param array $authedRoutes Routes requiring an authenticated user
     * @param array $viewMap
     *
     * @return type
     */
    public function go(array $publicRoutes, array $authedRoutes, array $viewMap)
    {
        // any problems here, we're done for. Need these to format errors!
        $Request  = $this->Factory->getRequest();
        $Renderer = $this->Factory->getRenderer();
        $Response = $this->Factory->getResponse();

        try {
            $Auth     = $this->Factory->getAuthentication();

            $Router   = $this->Factory->getRouter('Main');
            $AuthedRouter = $this->Factory->getRouter('Authenticated');
            $PublicRouter = $this->Factory->getRouter('Public');
            $MultiRouter  = $this->Factory->getRouter('Multipart');

            $PublicRouter->setRoutes($publicRoutes);
            $AuthedRouter->setRoutes($authedRoutes);

            $MultiRouter->addRouter($AuthedRouter);
            $MultiRouter->addRouter($PublicRouter);

            $Router->addRouter($MultiRouter);
            $Router->addRouter($AuthedRouter);
            $Router->addRouter($PublicRouter);

            $Renderer->setMapping($viewMap);

            $User = $Auth->getUser($Request);

        } catch (Exceptions\Base $e) {

            $User = $this->Factory->get('Models\\NullUser');
            return $this->handleException($e, $Renderer, $Request, $Response, $User);

        } catch (\Exception $e) {

            $User = $this->Factory->get('Models\\NullUser');
            return $this->handleError($e, $Renderer, $Request, $Response, $User);

        }

        try {
            $Workflows = $Router->route($Request, $User);

            if (is_null($Workflows)) {
                throw new Exceptions\RoutingException('No route found for ' . $Request->url(), 404);
            }

            foreach ($Workflows as $id => $Workflow) {
                $PartialRequest = $this->getWorkflowRequest($Request, $Response, $id);
                $Response->addOutput((array) $Workflow->run($PartialRequest, $User), get_class($Workflow), $id);
            }

            $View = $Renderer->render($Response, $Request, $User);

            return $View->render();

        } catch (Exceptions\Base $e) {

            return $this->handleException($e, $Renderer, $Request, $Response, $User);

        } catch (\Exception $e) {

            return $this->handleError($e, $Renderer, $Request, $Response, $User);

        }
    }

    /**
     * Handles exceptions we've thrown.
     *
     * @param \Exception $e
     */
    protected function handleException(
        \Exception $e,
        Interfaces\Renderer $Renderer,
        Interfaces\Request $Request,
        Interfaces\Response $Response,
        Interfaces\User $User
    ) {
        header('HTTP/1.1 ' . $e->HttpStatus() . ' ' . $e->HttpMessage());

        $e->log();

        $response = array(
            'success'   => false,
            'code'      => $e->getCode(),
            'message'   => $e->getMessage()
        );

        $Response->addOutput($response, 'error' . $e->getCode(), 'FatalError');

        $View = $Renderer->render($Response, $Request, $User);

        return $View->render();
    }

    /**
     * Handles unexpected exceptions
     *
     * @param \Exception $e
     */
    protected function handleError(
        \Exception $e,
        Interfaces\Renderer $Renderer,
        Interfaces\Request $Request,
        Interfaces\Response $Response,
        Interfaces\User $User
    ) {

        $response = array(
            'success'   => false,
            'code'      => '0000',
            'message'   => 'A fatal application error has occurred.'
        );

        error_log($e->getFile() . '::' . $e->getLine() . ': ' . $e->getMessage());
        header('HTTP/1.1 500 Application error');

        $Response->addOutput($response, 'error500', 'FatalError');

        $View = $Renderer->render($Response, $Request, $User);

        return $View->render();
    }

    protected function getWorkflowRequest(Interfaces\Request $Request, Interfaces\Response $Response, $routeId)
    {
        if ($Request->url() !== '/chained-request' || !$Request->has('requests')
            || !is_array($Request->get('requests'))
        ) {
            return $Request;
        }

        $partial = $this->getPartialRequest($Request, $routeId);

        $partial['post'] = $this->interpolate($Response, $partial['interpolate'], $partial['post']);

        return $this->Factory->getRequest($partial['uri'], $partial['post']);
    }

    protected function getPartialRequest(Interfaces\Request $Request, $routeId)
    {
        $partial = array(
            'uri'       => '',
            'post'      => array(),
            'interpolate' => array()
        );

        foreach ($Request->get('requests') as $tmp) {
            // find and expand partial request for current workflow
            if (isset($tmp['id']) && $tmp['id'] === $routeId) {
                $partial['uri']  = (isset($tmp['uri'])  && is_string($tmp['uri'])) ? $tmp['uri']  : '';
                $partial['post'] = (isset($tmp['post']) && is_array($tmp['post'])) ? $tmp['post'] : array();
                if (isset($tmp['interpolate']) && is_array($tmp['interpolate'])) {
                    $partial['interpolate'] = $tmp['interpolate'];
                }
            }
        }

        return $partial;
    }

    /**
     * If any data is needed from previous requests in order to run this one, this function will add it in.
     *
     * @param \MML\LetsGo\Interfaces\Response $Response
     * @param array $interpolate
     * @param array $requestData
     * @return array
     *
     * @throws Exceptions\LinkedRequestException
     */
    protected function interpolate(Interfaces\Response $Response, array $interpolate, array $requestData)
    {
        foreach ($interpolate as $newEntry) {

            if (!isset($newEntry['insertKey']) || !isset($newEntry['fromResponse'])
                || !isset($newEntry['valueLocation'])
            ) {
                throw new Exceptions\LinkedRequestException('Cannot interpolate value. Missing required paramaters');
            }
            if (!is_string($newEntry['insertKey']) || !is_string($newEntry['insertKey'])
                || !is_array($newEntry['valueLocation'])
            ) {
                throw new Exceptions\LinkedRequestException('Cannot interpolate value. Invalid paramaters provided');
            }

            $responseData = $Response->getById($newEntry['fromResponse']);

            foreach ($newEntry['valueLocation'] as $key) {
                if (!isset($responseData[$key])) {
                    throw new Exceptions\LinkedRequestException('Cannot find response value for interpolation');
                }
                $responseData = $responseData[$key];
            }

            $requestData[$newEntry['insertKey']] = $responseData;
        }

        return $requestData;
    }
}
