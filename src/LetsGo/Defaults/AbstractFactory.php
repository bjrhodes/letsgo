<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;
use MML\LetsGo\Models;
use MML\LetsGo\Routers;

/**
 * This is used so that you can create a factory which meets our required interfaces with minimal effort.
 *
 * @author barry
 */
abstract class AbstractFactory implements Interfaces\Factory
{
    protected $Parent;

    public function get($class, array $params = null)
    {
        return null;
    }
    public function getConfig()
    {
        return null;
    }
    public function getRequest($url = null, array $post = null, array $get = null, array $server = null)
    {
        return null;
    }
    public function getRouter($type = null)
    {
        return null;
    }
    public function getRenderer()
    {
        return null;
    }
    public function getResponse()
    {
        return null;
    }
    public function getAuthentication()
    {
        return null;
    }
    public function getFrontController()
    {
        return null;
    }
    public function getWorkflow($workflowName)
    {
        return null;
    }
    public function getView($viewName, $Request)
    {
        return null;
    }

    public function parentFactory(Interfaces\Factory $Parent = null)
    {
        if (!is_null($Parent)) {
            $this->Parent = $Parent;
        }

        return $this->Parent;
    }
}
