<?php
namespace MML\LetsGo\Defaults;

/**
 * Description of Response
 *
 * @author barry
 */
class Response implements \MML\LetsGo\Interfaces\Response
{
    private $out = array();

    private $position;

    public function __construct()
    {
        $this->position = 0;
    }

    public function addOutput(array $out, $workflowName, $requestIdentifier)
    {
        $this->out[] = array(
            'requestId'     => $requestIdentifier,
            'workflowName'  => $workflowName,
            'data'          => $out
        );
    }

    public function getById($id)
    {
        foreach ($this->out as $item) {
            if ($id === $item['requestId']) {
                return $item['data'];
            }
        }
    }

    public function output()
    {
        return $this->out[$this->position]['data'];
    }
    public function workflow()
    {
        return $this->out[$this->position]['workflowName'];
    }
    public function id()
    {
        return $this->out[$this->position]['requestId'];
    }
    public function count()
    {
        return count($this->out);
    }


    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this;
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->out[$this->position]);
    }
}
