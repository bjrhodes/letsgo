<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;
use MML\LetsGo\Models;
use MML\LetsGo\Routers;

/**
 * Description of Factory
 *
 * @author barry
 */
class Factory implements Interfaces\Factory
{
    protected $Config;
    protected $UserFactory;

    /**
     *
     * @param string $config json string
     * @param \MML\LetsGo\Interfaces\Factory $UserFactory
     */
    public function __construct(Interfaces\Config $Config, Interfaces\Factory $UserFactory = null)
    {
        $this->UserFactory  = $UserFactory;
        $this->Config       = $Config;

        if (!is_null($this->UserFactory) && method_exists($this->UserFactory, 'parentFactory')) {
            $this->UserFactory->parentFactory($this);
        }
    }

    public function getConfig()
    {
        return $this->Config;
    }

    public function getRouter($type = null)
    {
        $R = null;
        if (!is_null($this->UserFactory)) {
            $R = $this->UserFactory->getRouter($type);
        }

        if (is_null($R)) {
            switch ($type) {
                case 'Main':
                    $R = new Routers\Main();
                    break;
                case 'Authenticated':
                    $R = new Routers\Regex($this, new \MML\LetsGo\Specifications\IsAuthenticated());
                    break;
                case 'Public':
                    $R = new Routers\Regex($this, new \MML\LetsGo\Specifications\AlwaysAllow());
                    break;
                case 'Multipart':
                    $R = new Routers\Multipart($this, new \MML\LetsGo\Specifications\IsAuthenticated());
                    break;
            }
        }

        return $R;
    }

    public function getRenderer()
    {
        $R = null;
        if (!is_null($this->UserFactory)) {
            $R = $this->UserFactory->getRenderer();
        }
        if (is_null($R)) {
            $R = new Renderer($this);
        }
        return $R;
    }

    public function getAuthentication()
    {
        $A = null;
        if (!is_null($this->UserFactory)) {
            $A = $this->UserFactory->getAuthentication();
        }
        if (is_null($A)) {
            $A = new Authentication($this);
        }
        return $A;
    }

    public function getResponse()
    {
        $R = null;
        if (!is_null($this->UserFactory)) {
            $R = $this->UserFactory->getResponse();
        }
        if (is_null($R)) {
            $R = new Response($this);
        }
        return $R;
    }

    public function getFrontController()
    {
        $R = null;
        if (!is_null($this->UserFactory)) {
            $R = $this->UserFactory->getFrontController();
        }
        if (is_null($R)) {
            $R = new FrontController($this);
        }
        return $R;
    }

    public function getWorkflow($workflowName)
    {
        $W = null;
        if (!is_null($this->UserFactory)) {
            $W = $this->UserFactory->getWorkflow($workflowName);
        }
        if (is_null($W)) {
            if (strstr($workflowName, '\\')) {
                $name = $workflowName;
            } else {
                $name = '\\MML\\LetsGo\\Workflows\\' . $workflowName;
            }
            $W = new $name($this);
        }

        return $W;
    }

    public function getRequest($url = null, array $post = null, array $get = null, array $server = null)
    {
        $R = null;
        if (!is_null($this->UserFactory)) {
            $R = $this->UserFactory->getRequest();
        }

        if (is_null($R)) {

            $post   = (is_null($post))   ? $_POST   : $post;
            $get    = (is_null($get))    ? $_GET    : $get;
            $server = (is_null($server)) ? $_SERVER : $server;

            $url = (is_null($url) && isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : $url;

            $R = new Request($post, $get, $server, $url);
        }

        return $R;
    }

    public function getView($viewName, $Request)
    {
        $V = null;

        if (!is_null($this->UserFactory)) {
            $V = $this->UserFactory->getView($viewName, $Request);
        }

        if (is_null($V)) {
            $name = '\\MML\\LetsGo\\Views\\' . $viewName;

            if (strstr($name, '\\Html')) {
                $V = new $name($this);
            } else {
                $V = new $name($Request);
            }
        }

        return $V;
    }

    public function get($classname, array $params = null)
    {
        $Obj = null;

        if (!is_null($this->UserFactory)) {
            $Obj = $this->UserFactory->get($classname, $params);
        }

        if (is_null($Obj)) {
            $method = 'make' . str_replace('\\', '', $classname);
            if (method_exists($this, $method)) {
                $Obj = $this->$method();
            } else {
                throw new Exceptions\FactoryException('Cannot find object ' . $classname);
            }
        }

        return $Obj;
    }

    protected function makeModelsFileSystemTemplate()
    {
        return new Models\FilesystemTemplate();
    }

    protected function makeModelsNullUser()
    {
        return new Models\NullUser(new Models\NullPermissions());
    }
}
