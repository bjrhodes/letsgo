<?php
namespace MML\LetsGo\Defaults;

use MML\LetsGo\Interfaces;

/**
 * Description of Authentication
 *
 * @author barry
 */
class Authentication implements Interfaces\Authentication
{
    protected $Factory;

    public function __construct(Interfaces\Factory $Factory)
    {
        $this->Factory = $Factory;
    }

    /**
     * We have no default Auth method, so always return null user. This model should be over-ridden!
     *
     * Your Auth system should be responsible for inspecting the request and deciding if there is a valid user
     * in there. Either in the form of a login, or an existing session. We would expect this object to be a lightweight
     * interface, handing off to a session manager and / or user authentication plugin.
     *
     * @param \MML\LetsGo\Interfaces\Request $Request
     * @return \MML\LetsGo\Interfaces\User
     */
    public function getUser(Interfaces\Request $Request)
    {
        return $this->Factory->get('Models\\NullUser');
    }
}
