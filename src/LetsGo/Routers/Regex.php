<?php
namespace MML\LetsGo\Routers;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;

/**
 * Description of Regex
 *
 * @author barry
 */
class Regex implements Interfaces\Router
{
    protected $Factory;
    protected $Specification;

    protected $routes = array();

    /**
     * By Default, this router is created by our factory with a simple isAuthenticated Spec. If you need a more
     * complex spec, overload the getRouter() method in a custom factory and inject a different Spec.
     *
     * @param InterfacesFactory       $Factory [description]
     * @param InterfacesSpecification $Spec    [description]
     */
    public function __construct(Interfaces\Factory $Factory, Interfaces\Specification $Spec)
    {
        $this->Factory = $Factory;
        $this->Specification = $Spec;
    }

    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }

    public function route(Interfaces\Request $Request, Interfaces\User $User)
    {
        $url = strtolower($Request->barePath());
        $Workflows = null;

        foreach ($this->routes as $pattern => $workflowName) {
            if (preg_match($pattern, $url)) {
                $Workflows = array($this->Factory->getWorkflow($workflowName));
                break;
            }
        }

        // conceptually cleaner to run this test as a short-circuit, but then the endpoint can't distinguish
        // between 404 and 401. An important distinction!
        if ($this->Specification->isSatisfiedBy($User)) {
            return $Workflows;
        } elseif (is_null($Workflows)) {
            return null;
        } else {
            throw new Exceptions\AuthDenied('User not authorised for route.', '401');
        }
    }
}
