<?php
namespace MML\LetsGo\Routers;

use MML\LetsGo\Interfaces;

/**
 * Description of FrontController
 *
 * @author barry
 */
class Main implements Interfaces\Router
{
    protected $Routers = array();

    public function addRouter(Interfaces\Router $Router)
    {
        array_push($this->Routers, $Router);
    }

    public function route(Interfaces\Request $Request, Interfaces\User $User)
    {
        $Workflow = null;

        foreach ($this->Routers as $Router) {
            $Workflow = $Router->route($Request, $User);
            if (!is_null($Workflow)) {
                break;
            }
        }

        return $Workflow;
    }
}
