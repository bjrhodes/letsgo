<?php
namespace MML\LetsGo\Routers;

use MML\LetsGo\Interfaces;

/**
 * After being setup with sub-Routers, this will capture chained requests, split them into multiple requests
 * and route each sub-request returning an array of workflows.
 *
 * @author barry
 */
class Multipart implements Interfaces\Router
{
    protected $Factory;
    protected $Specification;
    protected $routers = array();

    protected $Routers = array();

    public function addRouter(Interfaces\Router $Router)
    {
        array_push($this->Routers, $Router);
    }

    public function __construct(Interfaces\Factory $Factory, Interfaces\Specification $Spec)
    {
        $this->Factory = $Factory;
        $this->Specification = $Spec;
    }

    public function route(Interfaces\Request $Request, Interfaces\User $User)
    {
        if (!$this->Specification->isSatisfiedBy($User)) {
            return null;
        }

        $partialRequests = $this->getPartialRequests($Request);

        if (!$partialRequests) {
            return null;
        }

        $this->User = $User;

        foreach ($partialRequests as $partial) {
            $PartialRequest = $this->Factory->getRequest($partial['uri'], $partial['post']);
            $Returned = $this->routePartial($PartialRequest);

            // nested routers will return an array of (1) workflows which we need to extract and label
            if (is_null($Returned)) {
                $Workflow = $this->Factory->getWorkflow('NotFound');
            } else {
                $Workflow = $Returned[0];
            }

            $Workflows[$partial['id']] = $Workflow;
        }

        return $Workflows;
    }

    protected function routePartial(Interfaces\Request $Request)
    {
        $Workflow = null;

        foreach ($this->Routers as $Router) {
            $Workflow = $Router->route($Request, $this->User);
            if (!is_null($Workflow)) {
                break;
            }
        }

        return $Workflow;
    }

    protected function getPartialRequests(Interfaces\Request $Request)
    {
        if ($Request->url() !== '/chained-request' || !$Request->has('requests')) {
            return false;
        }

        $requests = $Request->get('requests');

        if (!is_array($requests)) {
            return false;
        }

        return $this->normalise($requests);
    }

    protected function normalise($requests)
    {
        $i = 0;
        foreach ($requests as $key => $request) {
            if (!isset($request['uri'])) {
                return false;
            }
            $requests[$key]['id'] = isset($request['id']) ? $request['id'] : $i++;
            $requests[$key]['post'] = isset($request['post']) ? $request['post'] : array();
        }

        return $requests;
    }
}
