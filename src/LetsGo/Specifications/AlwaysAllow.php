<?php

namespace MML\LetsGo\Specifications;

use MML\LetsGo\Interfaces;

/**
 * Will always return true
 *
 * @author barry
 */
class AlwaysAllow implements Interfaces\Specification
{
    public function isSatisfiedBy(Interfaces\User $User)
    {
        return true;
    }
}
