<?php
namespace MML\LetsGo\Specifications;

use MML\LetsGo\Interfaces;

/**
 * Most basic authorisation, simply checks if the user is authenticated.
 *
 * Your own Specifications could, for example, query the User Permissions and return true or false.
 *
 * @author barry
 */
class IsAuthenticated implements Interfaces\Specification
{
    public function isSatisfiedBy(Interfaces\User $User)
    {
        // forbid null users, allow all other. We only need to test that the user is authenticated
        return !is_null($User->username());
    }
}
