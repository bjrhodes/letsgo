<?php
namespace MML\LetsGo\Models;

use MML\LetsGo\Interfaces;

/**
 * Description of NullUser
 *
 * @author barry
 */
class NullUser implements Interfaces\User
{
    protected $NullPermissions;

    public function __construct(NullPermissions $NullPermissions)
    {
        $this->NullPermissions = $NullPermissions;
    }

    public function username()
    {
        return null;
    }

    public function permissions($realm = null)
    {
        return $this->NullPermissions;
    }

    public function authToken($authToken = null)
    {
        return null;
    }
}
