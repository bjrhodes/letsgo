<?php
namespace MML\LetsGo\Models;

use MML\LetsGo\Interfaces;

/**
 * Description of NullPermissions
 *
 * @author barry
 */
class NullPermissions implements Interfaces\Permissions
{
    public function has($token)
    {
        return false;
    }
}
