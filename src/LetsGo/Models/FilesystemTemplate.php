<?php

namespace MML\LetsGo\Models;

use MML\LetsGo\Interfaces;
use MML\LetsGo\Exceptions;

/**
 * Used to render a template from the filesystem. Kepp it simple as possible as this is a pig to test.
 *
 * @author barry
 */
class FilesystemTemplate implements Interfaces\TemplateRenderer
{
    protected $filename;

    public function setTemplate($filename)
    {
        if (!file_exists($filename)) {
            throw new Exceptions\FileSystemException('Cannot find template to render: ' . $filename);
        }

        $this->filename = $filename;
    }

    public function render(array $vars)
    {
        extract($vars);

        ob_start();
        include $this->filename;
        return ob_get_clean();
    }
}
