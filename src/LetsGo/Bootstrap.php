<?php
namespace MML\LetsGo;

class Bootstrap
{
    /**
     *
     * @param array $routes
     * @param array $configvars
     * @param array $viewMap
     * @param \MML\LetsGo\Interfaces\Factory $Factory
     * @return type
     */
    public function run(
        array $routes,
        array $configvars,
        array $viewMap = null,
        Interfaces\Factory $Factory = null
    ) {

        if (is_null($viewMap)) {
            $viewMap = array(
                'error0' => array('layout' => 'error', 'template' => 'errors/500'),
                'error500' => array('layout' => 'error', 'template' => 'errors/500'),
                'error404' => array('layout' => 'error', 'template' => 'errors/404'),
                'error401' => array('layout' => 'error', 'template' => 'errors/401'),
            );
        }

        if (array_key_exists('public', $routes) || array_key_exists('authed', $routes)) {
            $public = array_key_exists('public', $routes) ? $routes['public'] : array();
            $authed = array_key_exists('authed', $routes) ? $routes['authed'] : array();
        } else {
            $public = $routes;
            $authed = array();
        }

        $Config  = new Defaults\Config($configvars);
        $Factory = new Defaults\Factory($Config, $Factory);

        $FC = $Factory->getFrontController();
        return $FC->go($public, $authed, $viewMap);
    }

    /**
     * Optional extra boot steps; sets up an error_handler
     *
     * @return null
     */
    public function init()
    {
        set_error_handler('\\MML\\LetsGo\\Bootstrap::errorHandler');
    }

    /**
     *
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline)
    {
        throw new \ErrorException($errstr, 500, $errno, $errfile, $errline);
    }
}
