<?php
namespace MML\LetsGo\Views;

use MML\LetsGo\Interfaces;

/**
 * Description of DownloadFile
 *
 * @author barry
 */
class DownloadFile extends ChunkFile implements Interfaces\View
{
    public function render()
    {
        header('Content-Disposition: attachment; filename="' . $this->details['name'] . '"');
        parent::render();
    }
}
