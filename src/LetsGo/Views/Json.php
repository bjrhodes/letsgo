<?php

namespace MML\LetsGo\Views;

use MML\LetsGo\Interfaces;

/**
 * Description of Html
 *
 * @author barry
 */
class Json implements Interfaces\View
{
    protected $vars = array();

    public function populate(array $contents)
    {
        $this->vars = $contents;
    }

    public function render()
    {
        return json_encode($this->vars);
    }
}
