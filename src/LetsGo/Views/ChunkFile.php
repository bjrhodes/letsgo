<?php
namespace MML\LetsGo\Views;

use MML\LetsGo\Interfaces;

/**
 * At the moment we only support single ranges.
 * Multiple ranges requires some more work to ensure it works correctly
 * and comply with the specifications: http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
 *
 * Multirange support annouces itself with:
 * header('Accept-Ranges: bytes');
 *
 * Multirange content must be sent with multipart/byteranges mediatype,
 * (mediatype = mimetype)
 * as well as a boundry header to indicate the various chunks of data.
 *
 * @author barry
 */
class ChunkFile implements Interfaces\View
{
    protected $details = array();
    protected $headers = array();
    protected $httpStatus = 'HTTP/1.1 200 OK';

    /**
     * @todo interface this up
     */
    public function __construct(Interfaces\Request $Request)
    {
        $this->Request = $Request;
    }

    /**
     * Populates the View with any parameters it will need.
     *
     * @param array $contents
     */
    public function populate(array $contents)
    {
        $this->details = $contents;
    }

    /**
     * Streams the file
     */
    public function render()
    {
        try {

            $this->run();

        } catch (\RangeException $e) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header('Content-Range: bytes 0-' . $this->details['size'] . '/' . $this->details['size']);
            return null;
        }
    }

    protected function run()
    {
        $this->headers['Content-Type']      = $this->details['mimetype'];

        if ($this->Request->has('HTTP_RANGE')) {
            list($start, $end) = $this->rangeDownload($this->details['size']);
        } else {
            $this->headers['Content-length']    = $this->details['size'];
            $start = 0;
            $end = $this->details['size'];
        }

        $this->outputHeaders();
        $this->outputFile($this->details['path'], $start, $end);

        return null;
    }

    protected function outputHeaders()
    {
        header($this->httpStatus);

        foreach ($this->headers as $key => $value) {
            header($key . ': ' . $value);
        }
    }

    protected function outputFile($path, $start, $end)
    {
        $fileStream = fopen($path, 'r');
        fseek($fileStream, $start);

        // pick a bufsize that makes you happy (8192 has been suggested).
        $bufsize = 8192;

        // here and now, this is a waste of cycles. Should fopen fail, an exception will be thrown, however
        // this is highly dependant on environment (errorHandler) so I've put this in as a failsafe because The while
        // loop will go infinite should fopen fail and it get that far.
        if (!$fileStream) {
            throw new \MacMate\Sharing\Exceptions\FileSystemException('File for streamview missing!');
        }

        $pointer = ftell($fileStream);

        while (!feof($fileStream) && $pointer <= $end) {

            if ($pointer + $bufsize > $end) {
                // truncate last chunk if it runs over end.
                $bufsize = $end - $pointer + 1;
            }

            echo fread($fileStream, $bufsize);
            flush();
        }

        fclose($fileStream);
    }

    protected function rangeDownload($fileSize)
    {
        // setup defaults
        $length = $fileSize;        // whole file
        $start  = 0;                // Start byte
        $end    = $fileSize - 1;    // End byte

        // Extract the range string
        list(, $range) = explode('=', $this->Request->get('HTTP_RANGE'), 2);

        // Make sure the client hasn't sent us a multibyte range
        if (strstr($range, ',')) {
            throw new \RangeException('Range Not Satisfiable');
        }

        // If the range starts with an '-' we start from the beginning
        // If not, we forward the file pointer
        // And make sure to get the end byte if specified
        if (strpos($range, '-') === 0) {
            // The n-number of the last bytes is requested
            $start = $fileSize - substr($range, 1);
        } else {
            $range = explode('-', $range);
            $start = $range[0];
            if (isset($range[1]) && is_numeric($range[1]) && $range[1] < $end) {
                $end = $range[1];
            }
        }

        // Validate the requested range and return an error if it's not correct.
        if ($start > $end || $end >= $fileSize) {
            throw new \RangeException('Range Not Satisfiable');
        }

        $length = $end - $start + 1; // Calculate new content length

        // Notify the client the byte range info
        $this->httpStatus = 'HTTP/1.1 206 Partial Content';
        $this->headers['Accept-Ranges']     ='0-$fileSize';
        $this->headers['Content-Range']     = "bytes $start-$end/$fileSize";
        $this->headers['Content-Length']    = $length;

        return array($start, $end);
    }
}
