<?php
namespace MML\LetsGo\Views;

use MML\LetsGo\Interfaces;

/**
 * Description of Html
 *
 * @author barry
 */
class Html implements Interfaces\View
{
    protected $Factory;
    protected $Config;

    protected $template;
    protected $layout;

    protected $vars = array();

    public function __construct(Interfaces\Factory $Factory)
    {
        $this->Factory = $Factory;
        $this->Config = $this->Factory->getConfig();
    }

    public function populate(array $contents)
    {
        $this->vars = $contents;
    }

    public function render()
    {
        if (!$this->template) {
            throw new \MML\LetsGo\Exceptions\Base('No template(s) specified for html view');
        }

        $defaults = array(
            'css'   => $this->Config->get('cssFiles'),
            'js'    => $this->Config->get('jsFiles'),
            'title' => $this->Config->get('siteTitle'),
            'cdn'   => $this->Config->get('cdn'),
        );

        $params = array_merge($defaults, $this->vars);

        $Template = $this->Factory->get('Models\\FileSystemTemplate');

        $Template->setTemplate($this->template);
        $params['mainTemplateContent'] = $Template->render($params);

        $Template->setTemplate($this->layout);

        return $Template->render($params);
    }

    public function template($templateName)
    {
        $this->template = $this->Config->get('templatesDir') . $templateName;
    }

    public function layout($layoutName)
    {
        $this->layout = $this->Config->get('layoutsDir') . $layoutName;
    }
}
